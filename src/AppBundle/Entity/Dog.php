<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Dog extends Pet
{
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $kennels;
}
