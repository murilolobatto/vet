<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Pet", mappedBy="owner", cascade={"all"})
     */
    private $pets;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->pets = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPets()
    {
        return $this->pets;
    }

    public function addPet(Pet $pet)
    {
        $pet->setOwner($this);

        $this->pets->add($pet);
    }

    /**
     * @param mixed $pets
     */
    public function setPets($pets)
    {
        $this->pets = $pets;
    }


}